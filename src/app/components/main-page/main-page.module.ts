import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { TranslateComponent } from './components/translate/translate.component';
import { WordDayComponent } from './components/word-day/word-day.component';
import { LastWorkedComponent } from './components/last-worked/last-worked.component';
import { StartPlayComponent } from './components/start-play/start-play.component';
import { WeekEtymologyComponent } from './components/week-etymology/week-etymology.component';
import { DictionaryComponent } from './pages/dictionary/dictionary.component';
import { RejectionWordsComponent } from './pages/rejection-words/rejection-words.component';
import { GrammarComponent } from './pages/grammar/grammar.component';
import { RejectionVerbComponent } from './pages/rejection-words/rejection-verb/rejection-verb.component';
import { RejectionNounComponent } from './pages/rejection-words/rejection-noun/rejection-noun.component';
import { ExpressionGrammarComponent } from './components/expression-grammar/expression-grammar.component';
import { WordsEndComponent } from './pages/words-end/words-end.component';
import { WordsFromLettersComponent } from './pages/words-from-letters/words-from-letters.component';
import { CrosswordAnswersComponent } from './pages/crossword-answers/crossword-answers.component';
import { NumeralsSpellingComponent } from './pages/numerals-spelling/numerals-spelling.component';
import { CurrencyConverterComponent } from './pages/currency-converter/currency-converter.component';
import { UkrainianNumeralsComponent } from './pages/numerals-spelling/ukrainian-numerals/ukrainian-numerals.component';
import { ItalianNumeralsComponent } from './pages/numerals-spelling/italian-numerals/italian-numerals.component';
import { FrenchNumeralsComponent } from './pages/numerals-spelling/french-numerals/french-numerals.component';
import { EnglishNumeralsComponent } from './pages/numerals-spelling/english-numerals/english-numerals.component';
import { ConnectionComponent } from './pages/connection/connection.component';
import { AboutComponent } from './pages/about/about.component';
import { VacanciesComponent } from './pages/vacancies/vacancies.component';
import { AgreementComponent } from './pages/agreement/agreement.component';
import { MainAboutComponent } from './pages/about/main-about/main-about.component';
import { OverviewDictionaryComponent } from './pages/about/overview-dictionary/overview-dictionary.component';
import { ListAbbreviationsComponent } from './pages/about/list-abbreviations/list-abbreviations.component';


@NgModule({
  declarations: [
    TranslateComponent,
    WordDayComponent,
    LastWorkedComponent,
    StartPlayComponent,
    WeekEtymologyComponent,
    DictionaryComponent,
    RejectionWordsComponent,
    GrammarComponent,
    RejectionVerbComponent,
    RejectionNounComponent,
    ExpressionGrammarComponent,
    WordsEndComponent,
    WordsFromLettersComponent,
    CrosswordAnswersComponent,
    NumeralsSpellingComponent,
    CurrencyConverterComponent,
    UkrainianNumeralsComponent,
    ItalianNumeralsComponent,
    FrenchNumeralsComponent,
    EnglishNumeralsComponent,
    ConnectionComponent,
    AboutComponent,
    VacanciesComponent,
    AgreementComponent,
    MainAboutComponent,
    OverviewDictionaryComponent,
    ListAbbreviationsComponent
  ],

  imports: [
    BrowserModule,
    TranslateModule.forRoot(),
  ],
  providers: [],
})
export class MainPageModule { }

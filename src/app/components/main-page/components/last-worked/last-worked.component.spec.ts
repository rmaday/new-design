import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastWorkedComponent } from './last-worked.component';

describe('LastWorkedComponent', () => {
  let component: LastWorkedComponent;
  let fixture: ComponentFixture<LastWorkedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastWorkedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastWorkedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

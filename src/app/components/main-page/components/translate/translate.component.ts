import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss']
})
export class TranslateComponent implements OnInit {
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() { }

  ngOnInit() {
  }

}

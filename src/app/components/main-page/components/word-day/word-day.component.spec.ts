import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordDayComponent } from './word-day.component';

describe('WordDayComponent', () => {
  let component: WordDayComponent;
  let fixture: ComponentFixture<WordDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpressionGrammarComponent } from './expression-grammar.component';

describe('ExpressionGrammarComponent', () => {
  let component: ExpressionGrammarComponent;
  let fixture: ComponentFixture<ExpressionGrammarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpressionGrammarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpressionGrammarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

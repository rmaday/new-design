import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekEtymologyComponent } from './week-etymology.component';

describe('WeekEtymologyComponent', () => {
  let component: WeekEtymologyComponent;
  let fixture: ComponentFixture<WeekEtymologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeekEtymologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekEtymologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

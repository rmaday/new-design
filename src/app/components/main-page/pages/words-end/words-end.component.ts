import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-words-end',
  templateUrl: './words-end.component.html',
  styleUrls: ['./words-end.component.scss']
})
export class WordsEndComponent implements OnInit {
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() { }

  ngOnInit() {
  }

}

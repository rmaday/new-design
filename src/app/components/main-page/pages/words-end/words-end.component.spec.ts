import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsEndComponent } from './words-end.component';

describe('WordsEndComponent', () => {
  let component: WordsEndComponent;
  let fixture: ComponentFixture<WordsEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordsEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordsEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

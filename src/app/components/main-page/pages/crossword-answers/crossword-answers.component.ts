import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crossword-answers',
  templateUrl: './crossword-answers.component.html',
  styleUrls: ['./crossword-answers.component.scss']
})
export class CrosswordAnswersComponent implements OnInit {
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() { }

  ngOnInit() {
  }

}

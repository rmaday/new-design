import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrosswordAnswersComponent } from './crossword-answers.component';

describe('CrosswordAnswersComponent', () => {
  let component: CrosswordAnswersComponent;
  let fixture: ComponentFixture<CrosswordAnswersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrosswordAnswersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrosswordAnswersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

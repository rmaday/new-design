import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumeralsSpellingComponent } from './numerals-spelling.component';

describe('NumeralsSpellingComponent', () => {
  let component: NumeralsSpellingComponent;
  let fixture: ComponentFixture<NumeralsSpellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumeralsSpellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumeralsSpellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

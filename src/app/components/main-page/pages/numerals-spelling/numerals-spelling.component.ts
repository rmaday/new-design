import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numerals-spelling',
  templateUrl: './numerals-spelling.component.html',
  styleUrls: ['./numerals-spelling.component.scss']
})
export class NumeralsSpellingComponent implements OnInit {
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() { }

  ngOnInit() {
  }

}

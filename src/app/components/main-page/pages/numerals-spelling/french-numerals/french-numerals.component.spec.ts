import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrenchNumeralsComponent } from './french-numerals.component';

describe('FrenchNumeralsComponent', () => {
  let component: FrenchNumeralsComponent;
  let fixture: ComponentFixture<FrenchNumeralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrenchNumeralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrenchNumeralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

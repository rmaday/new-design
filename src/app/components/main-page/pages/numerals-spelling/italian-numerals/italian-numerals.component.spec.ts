import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItalianNumeralsComponent } from './italian-numerals.component';

describe('ItalianNumeralsComponent', () => {
  let component: ItalianNumeralsComponent;
  let fixture: ComponentFixture<ItalianNumeralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItalianNumeralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItalianNumeralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnglishNumeralsComponent } from './english-numerals.component';

describe('EnglishNumeralsComponent', () => {
  let component: EnglishNumeralsComponent;
  let fixture: ComponentFixture<EnglishNumeralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnglishNumeralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnglishNumeralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

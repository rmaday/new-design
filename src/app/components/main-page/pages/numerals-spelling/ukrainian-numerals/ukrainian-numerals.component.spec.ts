import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UkrainianNumeralsComponent } from './ukrainian-numerals.component';

describe('UkrainianNumeralsComponent', () => {
  let component: UkrainianNumeralsComponent;
  let fixture: ComponentFixture<UkrainianNumeralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UkrainianNumeralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UkrainianNumeralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-currency-converter',
  templateUrl: './currency-converter.component.html',
  styleUrls: ['./currency-converter.component.scss']
})
export class CurrencyConverterComponent implements OnInit {
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() { }

  ngOnInit() {
  }

}

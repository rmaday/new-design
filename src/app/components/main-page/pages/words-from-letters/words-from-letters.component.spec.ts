import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsFromLettersComponent } from './words-from-letters.component';

describe('WordsFromLettersComponent', () => {
  let component: WordsFromLettersComponent;
  let fixture: ComponentFixture<WordsFromLettersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordsFromLettersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordsFromLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-words-from-letters',
  templateUrl: './words-from-letters.component.html',
  styleUrls: ['./words-from-letters.component.scss']
})
export class WordsFromLettersComponent implements OnInit {
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectionNounComponent } from './rejection-noun.component';

describe('RejectionNounComponent', () => {
  let component: RejectionNounComponent;
  let fixture: ComponentFixture<RejectionNounComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectionNounComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectionNounComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rejection-noun',
  templateUrl: './rejection-noun.component.html',
  styleUrls: ['./rejection-noun.component.scss']
})
export class RejectionNounComponent implements OnInit {

  public selectedTab = 'first';
  constructor() { }

  ngOnInit() {
  }

  selectTab(tab) {
    this.selectedTab = tab;
  }
}

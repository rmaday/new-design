import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectionVerbComponent } from './rejection-verb.component';

describe('RejectionVerbComponent', () => {
  let component: RejectionVerbComponent;
  let fixture: ComponentFixture<RejectionVerbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectionVerbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectionVerbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

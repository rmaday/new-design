import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rejection-words',
  templateUrl: './rejection-words.component.html',
  styleUrls: ['./rejection-words.component.scss']
})
export class RejectionWordsComponent implements OnInit {

  public rejectionNoun;
  public rejectionVerb = true;
  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectionWordsComponent } from './rejection-words.component';

describe('RejectionWordsComponent', () => {
  let component: RejectionWordsComponent;
  let fixture: ComponentFixture<RejectionWordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectionWordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectionWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewDictionaryComponent } from './overview-dictionary.component';

describe('OverviewDictionaryComponent', () => {
  let component: OverviewDictionaryComponent;
  let fixture: ComponentFixture<OverviewDictionaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewDictionaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewDictionaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

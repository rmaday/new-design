import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-abbreviations',
  templateUrl: './list-abbreviations.component.html',
  styleUrls: ['./list-abbreviations.component.scss']
})
export class ListAbbreviationsComponent implements OnInit {

  public selectedLetter = 'А';
  public alphabitArray = ['А', 'Б', 'В', 'Г', 'Ґ', 'Д', 'Е', 'Є', 'Ж', 'З', 'И', 'І', 'Ї', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ю', 'Я', 'Ь'];
  constructor() { }

  ngOnInit() {
  }

  selectLetter(letter) {
    this.selectedLetter = letter;
  }

}

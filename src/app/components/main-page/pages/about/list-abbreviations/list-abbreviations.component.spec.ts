import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAbbreviationsComponent } from './list-abbreviations.component';

describe('ListAbbreviationsComponent', () => {
  let component: ListAbbreviationsComponent;
  let fixture: ComponentFixture<ListAbbreviationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAbbreviationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAbbreviationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

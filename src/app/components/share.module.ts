import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [],

  imports: [
    BrowserModule,
    TranslateModule.forRoot(),
  ],
  providers: [],
})
export class ShareModule { }

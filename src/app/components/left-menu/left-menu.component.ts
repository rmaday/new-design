import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {
  @Output() menuBlockClose = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  closeMenu() {
    this.menuBlockClose.emit(false);
  }
}

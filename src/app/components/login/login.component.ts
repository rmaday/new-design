import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() popupLogin = new EventEmitter();
  @Output() registerUserPopup = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  closePopup() {
    this.popupLogin.emit(false);
  }

  registerUser() {
    this.registerUserPopup.emit(true);
  }
}

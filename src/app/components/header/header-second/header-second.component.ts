import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-header-second',
  templateUrl: './header-second.component.html',
  styleUrls: ['./header-second.component.scss']
})
export class HeaderSecondComponent implements OnInit {
  @Output() menuFromTop = new EventEmitter();
  public languages = ['Українська', 'Італійська', 'Французька', 'Англійська'];
  public registerUser;
  public popupLogin;
  public openSelector;
  public defaultLanguage = 'Українська';
  constructor() {}

  ngOnInit() {
  }

  menuOpen() {
    this.menuFromTop.emit(true);
  }
  openLoginPopup() {
    this.popupLogin =true;
  }
  closePopup(event) {
    this.popupLogin = event;
    this.registerUser = false;
  }
  registerUserPopup(event) {
    this.popupLogin = false;
    this.registerUser = event;
  }
  popupRegister(event) {
    this.registerUser = event;
  }
  loginPopupFromRegister(event) {
    this.popupLogin = event;
    this.registerUser = false;
  }
}

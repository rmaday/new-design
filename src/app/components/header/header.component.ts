import {Component, OnInit, AfterContentChecked, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params  } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterContentChecked {
  public popupLogin;
  public routersMainMenu = ['/', '/grammar','/words-end','/words-from-letters',
    '/crossword-answers','/numerals-spelling', '/currency-converter',
    '/connection', '/about','/vacancies', '/agreement'];
  public routersSecondTypeMenu = ['/rejection-words'];
  public openLeftMenu;
  public routeUrl;
  public registerUser;
  constructor(
    private router: Router, private route: ActivatedRoute
  ) {}

  ngOnInit() {
  }

  menuBlockClose(event) {
    this.openLeftMenu = event;
  }

  menuFromTop(event) {
    this.openLeftMenu = event;
  }

  ngAfterContentChecked() {
    this.routeUrl = this.router.url;
  }

  openLoginPopup() {
    this.popupLogin =true;
  }
  closePopup(event) {
    this.popupLogin = event;
    this.registerUser = false;
  }
  registerUserPopup(event) {
    this.popupLogin = false;
    this.registerUser = event;
  }
  popupRegister(event) {
    this.registerUser = event;
  }
  loginPopupFromRegister(event) {
    this.popupLogin = event;
    this.registerUser = false;
  }
}

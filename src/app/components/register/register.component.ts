import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Output() popupRegister = new EventEmitter();
  @Output() loginPopupFromRegister = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  closePopup() {
      this.popupRegister.emit(false);
  }

  goToLogin() {
    this.loginPopupFromRegister.emit(true);
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from './core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppRoutingModule } from './app.routing.module';
import { ShareModule } from './components/share.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { LeftMenuComponent } from './components/left-menu/left-menu.component';
import { MainPageModule } from './components/main-page/main-page.module';
import { DictionaryComponent } from './components/main-page/pages/dictionary/dictionary.component';
import { RejectionWordsComponent } from './components/main-page/pages/rejection-words/rejection-words.component';
import { GrammarComponent } from './components/main-page/pages/grammar/grammar.component';
import { HeaderSecondComponent } from './components/header/header-second/header-second.component';
import { WordsEndComponent } from './components/main-page/pages/words-end/words-end.component';
import { WordsFromLettersComponent } from './components/main-page/pages/words-from-letters/words-from-letters.component';
import { CrosswordAnswersComponent } from './components/main-page/pages/crossword-answers/crossword-answers.component';
import { NumeralsSpellingComponent } from './components/main-page/pages/numerals-spelling/numerals-spelling.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CurrencyConverterComponent } from './components/main-page/pages/currency-converter/currency-converter.component';
import { ConnectionComponent } from './components/main-page/pages/connection/connection.component';
import { AboutComponent } from './components/main-page/pages/about/about.component';
import { VacanciesComponent } from './components/main-page/pages/vacancies/vacancies.component';
import { AgreementComponent } from './components/main-page/pages/agreement/agreement.component';

const routes: Routes = [
  { path: '',                component: DictionaryComponent },
  { path: 'rejection-words', component: RejectionWordsComponent },
  { path: 'grammar',         component: GrammarComponent },
  { path: 'words-end',       component: WordsEndComponent },


  { path: 'words-from-letters',      component: WordsFromLettersComponent },
  { path: 'crossword-answers',       component: CrosswordAnswersComponent },
  { path: 'numerals-spelling',       component: NumeralsSpellingComponent },
  { path: 'currency-converter',       component: CurrencyConverterComponent },
  { path: 'connection',       component: ConnectionComponent },
  { path: 'about',            component: AboutComponent },
  { path: 'vacancies',        component: VacanciesComponent },
  { path: 'agreement',        component: AgreementComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HeaderSecondComponent,
    LeftMenuComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    TranslateModule.forRoot(),
    CoreModule,
    AppRoutingModule,
    MainPageModule,
    ShareModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

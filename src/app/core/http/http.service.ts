import 'rxjs/add/observable/throw';
import { Injectable } from '@angular/core';
import {
  Http, ConnectionBackend, RequestOptions, Request, Response, RequestOptionsArgs, RequestMethod, Headers
} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { extend } from 'lodash';
import { environment } from '../../../environments/environment';


@Injectable()
export class HttpService extends Http {
  constructor(backend: ConnectionBackend,
              private defaultOptions: RequestOptions) {

    super(backend, defaultOptions);
  }

  setAuthorizationToken() {
    const headers = new Headers();
    headers.append('content-type', 'application/json');
    const options = new RequestOptions({headers: headers});
    const token = localStorage.getItem('token');
    if (token) {
      headers.append('Authorization', 'Bearer ' +  token);
      return options;
    }

    return options;
  }
  request(request: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    options = options || {};
    let url: string;

    if (typeof request === 'string') {
      url = request;
      request = environment.serverUrl + url;
    } else {
      url = request.url;
      request.url = environment.serverUrl + url;
    }

    return this.httpRequest(request, options);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, this.setAuthorizationToken(), {method: RequestMethod.Get}))
      .catch((error: any) => {
        if (error.status === 401) {
          // localStorage.clear();
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          localStorage.removeItem('geoadmin');
          localStorage.removeItem('project');
          localStorage.removeItem('projectId');
          localStorage.removeItem('loadingLayers');
          localStorage.removeItem('permissionsProject');
          localStorage.removeItem('appRoles');
        }
        return Observable.throw(error);
      });
  }

  post(url: string, body, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, this.setAuthorizationToken(), {
      body:  body,
      method: RequestMethod.Post
    })).catch((error: any) => {
      return Observable.throw(error);
    });
  }

  put(url: string, body, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, this.setAuthorizationToken(), {
      body: body,
      method: RequestMethod.Put
    })).catch((error: any) => {
      return Observable.throw(error);
    });
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, this.setAuthorizationToken(), { method: RequestMethod.Delete }))
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, this.setAuthorizationToken(), {
      body: body,
      method: RequestMethod.Patch
    })).catch((error: any) => {
      return Observable.throw(error);
    });
  }

  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, { method: RequestMethod.Head }));
  }

  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, { method: RequestMethod.Options }));
  }

  private httpRequest(request: string|Request, options: RequestOptionsArgs): Observable<Response> {
    const req = super.request(request, options);
    return req;
  }

  // private errorHandler(response: Response): Observable<Response> {
  //   if (environment.production) {
  //     log.error('Request error', response);
  //     return Observable.throw(response);
  //   }
  //   throw response;
  // }

}

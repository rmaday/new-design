import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule, Http, XHRBackend, ConnectionBackend, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
// import { AuthenticationService } from './authentication/authentication.service';
import { HttpService } from './http/http.service';
import { I18nService } from './i18n.service';

export function createHttpService(backend: ConnectionBackend,
                                  defaultOptions: RequestOptions) {

  return new HttpService(backend, defaultOptions);
}


@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpModule,
    RouterModule,
    TranslateModule
  ],
  providers: [
    // AuthenticationService,
    HttpService,
    I18nService,
    {
      provide: Http,
      deps: [XHRBackend, RequestOptions],
      useFactory: createHttpService
    }],
  bootstrap: []
})
export class CoreModule { }
